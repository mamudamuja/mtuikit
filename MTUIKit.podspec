Pod::Spec.new do |s|
  s.name             = 'MTUIKit'
  s.version          = '0.1.0'
  s.summary          = 'A short description of MTUIKit.'
  s.description      = 'description ...'
  s.homepage         = 'https://github.com/tomicmilutin/MTUIKit'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'tomicmilutin' => 'milutin@appbuilder.at' }
  s.source           = { :git => 'https://github.com/tomicmilutin/MTUIKit.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.source_files = 'MTUIKit/Classes/**/*'
  
  # s.resource_bundles = {
  #   'MTUIKit' => ['MTUIKit/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
