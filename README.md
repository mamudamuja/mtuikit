# MTUIKit

[![CI Status](http://img.shields.io/travis/tomicmilutin/MTUIKit.svg?style=flat)](https://travis-ci.org/tomicmilutin/MTUIKit)
[![Version](https://img.shields.io/cocoapods/v/MTUIKit.svg?style=flat)](http://cocoapods.org/pods/MTUIKit)
[![License](https://img.shields.io/cocoapods/l/MTUIKit.svg?style=flat)](http://cocoapods.org/pods/MTUIKit)
[![Platform](https://img.shields.io/cocoapods/p/MTUIKit.svg?style=flat)](http://cocoapods.org/pods/MTUIKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MTUIKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MTUIKit'
```

## Author

tomicmilutin, milutin@appbuilder.at

## License

MTUIKit is available under the MIT license. See the LICENSE file for more info.
