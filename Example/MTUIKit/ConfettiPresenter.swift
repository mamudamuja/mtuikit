//
//  ConfettiPresenter.swift
//  MTUIKit_Example
//
//  Created by Milutin Tomic on 09.03.19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import MTUIKit
import MTNotificationCenter

/// This class implements the MTNotificationPresenter protocol
/// and serves as a presenter for MTNotifications
open class ConfettiPresenter: MTNotificationPresenter {
  
  // API
  
  /// Default initializer
  public init(){}
  
  /// Indicates whether the presenter is currently presenting a notification
  public var isPresenting: Bool {
    get {
      return self._isPresenting
    }
  }
  
  /// Returns the color of the shadow displayed beneath the notification container view.
  /// Override in subclass to achieve a different style.
  ///
  /// - Returns: shadow color
  open func shadowColor() -> UIColor {
    return UIColor.black.withAlphaComponent(0.3)
  }
  
  /// Returns the button icon displayed in the close button of the presenter.
  /// Override in subclass to achieve a different style.
  ///
  /// - Returns: button icon instance
  open func closeButtonIcon() -> UIImage? {
    let podBundle = Bundle.init(for: MTTopDrawerNotificationPresenter.self)
    let buttonIcon = UIImage.init(
      named: "MTNotificationCenter.bundle/closeButton",
      in: podBundle, compatibleWith: nil)
    return buttonIcon
  }
  
  /// Returns the 'dimColor' of the presenter. This is the color of the
  /// overlay covering the view in which the notification is shown.
  /// Override in subclass to achieve a different style.
  ///
  /// - Parameter forNotificationType: the notification type
  /// - Returns: dim color
  open func dimColor(forNotificationType: MTNotificationType) -> UIColor {
    return UIColor.black.withAlphaComponent(0.4)
  }
  
  /// This presenter adapts the notification container's color specific to the notification type.
  /// Override in subclass to achieve a different style.
  ///
  /// - Parameter forNotificationType: notification type
  /// - Returns: color instance
  open func color(forNotificationType: MTNotificationType) -> UIColor {
    switch forNotificationType {
    case .error:
      return #colorLiteral(red: 0.9529411765, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
    case .warning:
      return #colorLiteral(red: 0.9803921569, green: 0.737254902, blue: 0.1176470588, alpha: 1)
    case .success:
      return #colorLiteral(red: 0.05490196078, green: 0.7450980392, blue: 0.4901960784, alpha: 1)
    case .info:
      return #colorLiteral(red: 0.2156862745, green: 0.5607843137, blue: 0.9294117647, alpha: 1)
    }
  }
  
  /// Presents the passed notification to the user.
  ///
  /// - Parameters:
  ///   - notification: the notification
  ///   - inView: the view in which the notification will be shown.
  ///             the presenter will show the notification in the window directly,
  ///             as a default if this parameter is not specified.
  ///   - sourceViewController: the viewController presenting the notification.
  ///                           this is only important if you wish to update 'statusBarStyle'
  ///                           during the notification.
  ///   - animated: indicates wheter the presenter should animate the notification presentation
  public func present(
    notification: MTNotification,
    inView: UIView? = nil,
    sourceViewController: UIViewController? = nil,
    animated: Bool = true) {
    
    // create auto dismiss task
    self.dismissTask?.cancel()
    self.dismissTask = DispatchWorkItem { [weak self] in
      self?.dismiss(animated: animated)
    }
    
    // determine the target view
    guard let targetView = inView ?? UIApplication.shared.keyWindow else {
      return
    }
    
    // update source view controller
    self._isPresenting = true
    self.sourceViewController = sourceViewController
    sourceViewController?.setNeedsStatusBarAppearanceUpdate()
    
    // present notification
    backgroundView.dimColor = self
      .dimColor(forNotificationType: notification.type)
    targetView.addSubview(self.backgroundView)
    self.backgroundView.pinEdges()
    
    // notification content
    let contentView = notification.content.contentView()
    self.contentContainer.removeAllSubviews()
    self.contentContainer.addSubview(contentView)
    contentView.pinEdges()
    
    // show/hide close button
    self.closeButton.isHidden = !notification.isCancelable
    
    // setup initial constraints
    self.backgroundView.blockUI = false
    
    // setup view style
    self.dismissIndicator.isHidden = notification.duration == nil
    notificationContainer.backgroundColor =
      self.color(forNotificationType: notification.type)
    topPaddingView.backgroundColor =
      self.color(forNotificationType: notification.type)
    shadowView.shadowColor = self.shadowColor()
    
    self.notificationContainer.layoutIfNeeded()
    topConstraint?.constant = -notificationContainer.frame.height
    dimissIndicatorTrailingConstraint?.constant =
      notification.duration == 0 ? -notificationContainer.frame.width : 0
    backgroundView.layoutIfNeeded()
    backgroundView.layoutSubviews()
    
    // animate
    topConstraint?.constant = UIApplication.shared.statusBarFrame.height
    UIView.animate(
      withDuration: 0.5,
      delay: 0,
      usingSpringWithDamping: 0.7,
      initialSpringVelocity: 0.5,
      options: [.beginFromCurrentState, .allowUserInteraction],
      animations: {
        self.backgroundView.layoutIfNeeded()
        self.backgroundView.blockUI = notification.blocksUI
    }, completion: { (finished) in
      if finished {
        // trigger auto dismiss if notification has a specified duration
        if notification.duration != nil,
          notification.duration != 0,
          let task = self.dismissTask {
          // trigger dismissal task
          DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(notification.duration ?? 0),
            execute: task)
          
          // start dismiss indicator animation
          self.dimissIndicatorTrailingConstraint?.constant = 0
          self.notificationContainer.layoutIfNeeded()
          self.notificationContainer.layoutSubviews()
          self.dimissIndicatorTrailingConstraint?.constant =
            -self.notificationContainer.frame.width
          UIView.animate(
            withDuration: Double(notification.duration ?? 0),
            animations: {
              self.notificationContainer.layoutIfNeeded()
          })
        }
      }
    })
  }
  
  /// Dismisses the currently presented notification.
  ///
  /// - Parameter animated: indicates wheter the presenter should animate the dismissal
  public func dismiss(animated: Bool = true) {
    self.dismissTask?.cancel()
    
    // animate
    self.topConstraint?.constant = -notificationContainer.frame.height
    UIView.animate(withDuration: 0.3, animations: {
      self.backgroundView.layoutIfNeeded()
      self.backgroundView.blockUI = false
    }, completion: { (_) in
      self._isPresenting = false
      self.sourceViewController?.setNeedsStatusBarAppearanceUpdate()
      self.backgroundView.removeFromSuperview()
    })
  }
  
  // MARK: - Private
  
  private var dismissTask: DispatchWorkItem?
  
  private var sourceViewController: UIViewController?
  
  private var _isPresenting: Bool = false
  
  @objc private func closeButtonAction() {
    self.dismiss(animated: true)
  }
  
  private lazy var backgroundView: MTNotificationPresenterBackgroundView = {
    let view = MTNotificationPresenterBackgroundView.init()
    view.translatesAutoresizingMaskIntoConstraints = false
    view.clipsToBounds = true
    
    
    
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
      let confettiView = MTConfettiView.init(frame: CGRect.zero)
      view.addSubview(confettiView)
      view.sendSubview(toBack: confettiView)
      confettiView.pinEdges()
      
      confettiView.particles = [#imageLiteral(resourceName: "confetti4"), #imageLiteral(resourceName: "confetti6"), #imageLiteral(resourceName: "confetti3"), #imageLiteral(resourceName: "confetti2"), #imageLiteral(resourceName: "confetti7"), #imageLiteral(resourceName: "confetti1"), #imageLiteral(resourceName: "confetti5")]
      confettiView.emissionOrigin = CGRect.init(
        x: 0,
        y: 0,
        width: 375,
        height: 10)
      confettiView.emit()
    })
    
    
    view.addSubview(shadowView)
    view.addSubview(notificationContainer)
    
    // setup autolayout
    notificationContainer.pinLeadingToSuperView()
    notificationContainer.pinTrailingToSuperView()
    shadowView.pinLeadingToSuperView()
    shadowView.pinTrailingToSuperView()
    shadowView.constraintHeight(height: 10)
    view.addConstraint(NSLayoutConstraint.init(
      item: shadowView,
      attribute: .bottom,
      relatedBy: .equal,
      toItem: notificationContainer,
      attribute: .bottom,
      multiplier: 1,
      constant: 0))
    
    topConstraint = NSLayoutConstraint.init(
      item: notificationContainer, attribute: .top,
      relatedBy: .equal,
      toItem: view, attribute: .top,
      multiplier: 1,
      constant: 0)
    
    if let constraint = topConstraint {
      notificationContainer.superview?.addConstraint(constraint)
    }
    
    return view
  }()
  
  /// notification content is added directly to this view.
  /// the contentView already has sufficient padding to
  /// the notificationContainer.
  private lazy var contentContainer: UIView = {
    let view = UIView.init()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  private lazy var closeButton: UIButton = {
    let button = UIButton.init(type: .custom)
    button.translatesAutoresizingMaskIntoConstraints = false
    button.addTarget(
      self,
      action: #selector(closeButtonAction),
      for: .touchUpInside)
    button.backgroundColor = UIColor.clear
    button.setImage(self.closeButtonIcon(), for: .normal)
    return button
  }()
  
  private lazy var dismissIndicator: UIView = {
    let view = UIView.init()
    view.translatesAutoresizingMaskIntoConstraints = false
    view.backgroundColor = UIColor.white
    return view
  }()
  
  /// reference to the constraint that pins the top of the
  /// notificationContainer to the top of the presenting view
  private var topConstraint: NSLayoutConstraint?
  
  /// reference to the constraint that pins the dismiss indicator
  /// to the right edge of the notificationContainer
  private var dimissIndicatorTrailingConstraint: NSLayoutConstraint?
  
  /// this subview goes beyond the bounds of the container
  /// with the purpose to fill the status bar so that the
  /// notification does not look cut off
  private lazy var topPaddingView: UIView = {
    let view = UIView.init()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  private lazy var shadowView: UIView = {
    let shadowView = UIView.init()
    shadowView.translatesAutoresizingMaskIntoConstraints = false
    shadowView.shadowOffset = CGSize.init(width: 0, height: 0)
    shadowView.shadowRadius = 8
    shadowView.shadowOpacity = 1
    shadowView.shadowColor = UIColor.black.withAlphaComponent(0.3)
    return shadowView
  }()
  
  /// the notification container view displays
  /// the entire notification content
  private lazy var notificationContainer: UIView = {
    let view = UIView.init()
    view.translatesAutoresizingMaskIntoConstraints = false
    
    // add content view
    view.addSubview(self.contentContainer)
    self.contentContainer.pinEdges(insets:
      UIEdgeInsets.init(top: 35, left: 20, bottom: 20, right: 20))
    
    // add dismiss button
    view.addSubview(closeButton)
    closeButton.pinTopToSuperView(padding: 3)
    closeButton.pinTrailingToSuperView(padding: 20)
    
    // add top padding
    view.addSubview(topPaddingView)
    topPaddingView.pinLeadingToSuperView()
    topPaddingView.pinTrailingToSuperView()
    topPaddingView.pinTopToSuperView(padding: -200)
    topPaddingView.constraintHeight(height: 200)
    
    // add dismiss indicator
    view.addSubview(dismissIndicator)
    dismissIndicator.pinLeadingToSuperView()
    dismissIndicator.pinBottomToSuperView()
    dismissIndicator.constraintHeight(height: 2)
    
    dimissIndicatorTrailingConstraint = NSLayoutConstraint.init(
      item: dismissIndicator, attribute: .trailing,
      relatedBy: .equal,
      toItem: view, attribute: .trailing,
      multiplier: 1,
      constant: 0)
    
    if let constraint = dimissIndicatorTrailingConstraint {
      view.addConstraint(constraint)
    }
    
    return view
  }()
  
}
