//
//  TestViewController.swift
//  MTUIKit_Example
//
//  Created by Milutin Tomic on 16.03.19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import MTUIKit

class TestViewController: UIViewController {
  
  @IBOutlet weak var testLabel: MTLabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
  }
  
  @IBAction func toggleAction(_ sender: MTToggle) {
    print(sender.isOn)
  }
  
  
}
