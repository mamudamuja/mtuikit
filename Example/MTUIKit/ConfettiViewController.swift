//
//  ConfettiViewController.swift
//  MTUIKit_Example
//
//  Created by Milutin Tomic on 09.03.19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import MTUIKit
import MTNotificationCenter

class ConfettiViewController: MTNotificationPresenterViewController {
  
  @IBOutlet weak var mtLabel: MTLabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.presenter = ConfettiPresenter.init()
  }
  
  @IBAction func testButton(_ sender: UIButton) {
    let content = MTActionsNotificationContent.init(title: "Title", message: "Message.", actions: nil)
    let notification = MTNotification.init(content: content, type: .success, duration: nil)
    self.presenter.present(notification: notification, inView: nil, sourceViewController: self, animated: true)
  }
  
}
