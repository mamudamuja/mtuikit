//
//  UIView+Nib.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 27.02.19.
//

import UIKit

extension UIView {

  func loadNib() {
    let name = "\(self)"
    Bundle(for: type(of: self))
      .loadNibNamed(name, owner: self, options: nil)
  }
  
}
