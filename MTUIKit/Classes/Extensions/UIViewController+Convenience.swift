//
//  UIViewController+Storyboard
//  MTUIKit
//
//  Created by Milutin Tomic on 28.06.18.
//

import Foundation
import UIKit

/// This extension provides methods that cover some
/// common behaviours you want from a view controller
extension UIViewController {
    
    // MARK: - Storyboard Initialization
    
    /// Returns an instance of the view controller by initializing
    /// it from the storyboard with the passed name.
    ///
    /// NOTE: set the viewcontroller class name as the storyboard identifier
    ///
    /// - Parameter name: storyboard name, defaults to "Main"
    /// - Returns: instance of the view controller
    public class func makeFromStoryboard(name: String = "Main") -> Self {
        return instantiateFromStoryboard(name: name)
    }
    
    class func instantiateFromStoryboard<T>(name: String) -> T {
        return UIStoryboard.init(name: name, bundle: nil)
            .instantiateViewController(withIdentifier: String(describing: self)) as! T
    }
    
    // MARK: - Storyboard Transitions
    
    /// Transitions to a different rootViewController mimicking a simple modal dismiss animation
    ///
    /// - Parameter view: the view that should be animated of the screen
    public func replaceRootViewController(viewController: UIViewController?) {
        if let rootVC = viewController,
            let _ = rootVC.view {
            // create snapshot of current vc
            let snapshot = self.view.snapshot()
            
            // set new rootVC
            UIApplication.shared.keyWindow?.rootViewController = rootVC
            
            // add snapshot on top
            let imageView = UIImageView.init(image: snapshot)
            imageView.frame = UIApplication.shared.keyWindow?.bounds ?? CGRect.zero
            UIApplication.shared.keyWindow?.addSubview(imageView)
            
            // animate snapshot
            UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
                imageView.frame =
                    imageView.frame.offsetBy(dx: 0, dy: imageView.frame.size.height * 1.5)
            }) { (_) in
                // clean up
                imageView.removeFromSuperview()
            }
        }
    }
    
    // MARK: - Navigation Controller
    
    /// Returns an instance of the view controller
    /// embeded in an navigation controller
    ///
    /// - Returns: UINavigationController with caller as the root view controller
    public func embedInNavigationController() -> UINavigationController {
        return UINavigationController.init(rootViewController: self)
    }
    
}
