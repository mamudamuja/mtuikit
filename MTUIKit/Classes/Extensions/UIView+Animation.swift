//
//  UIView+Animation.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 28.02.19.
//

import UIKit

public extension UIView {
	
    func moveAndReturn(offset: CGPoint, repeatCount: Int? = nil) {
        self.transform = .identity
        let transform = self.transform.translatedBy(x: offset.x, y: offset.y)
        UIView.animate(withDuration: 1.0, delay: 0, options: [.autoreverse, .repeat, .curveEaseInOut], animations: {
            self.transform = transform
        }) { (finished) in
            
        }
    }
    
	/// Adds a little shake animation to the view
	func shake(repeatCount: Int = 4, offset: Float = 5.0) {
		let animation = CABasicAnimation.init(keyPath: "position")
		animation.duration = 0.05
		animation.repeatCount = Float(repeatCount)
		animation.autoreverses = true
		animation.fromValue = NSValue.init(cgPoint:
			CGPoint.init(x: self.center.x - CGFloat(offset), y: self.center.y))
		animation.toValue = NSValue.init(cgPoint:
			CGPoint.init(x: self.center.x + CGFloat(offset), y: self.center.y))
		self.layer.add(animation, forKey: "position")
	}
	
	/// Animates the views background color to red and back
	func flashRed(duration: Double = 0.4) {
		let oldColor = self.backgroundColor
		UIView.animate(withDuration: duration/2.0, animations: {
			self.backgroundColor = #colorLiteral(red: 0.9433749318, green: 0.3559464812, blue: 0.2664132416, alpha: 0.3775858275)
		}, completion: { _ in
			UIView.animate(withDuration: duration/2.0, animations: {
				self.backgroundColor = oldColor
			})
		})
	}
	
}
