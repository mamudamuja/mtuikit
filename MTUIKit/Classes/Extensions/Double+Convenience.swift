//
//  Double+Convenience.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 21.02.20.
//

import Foundation

public extension Double {
    
    var inRadians: Double {
        return Double.pi * self / 180.0
    }
    
}
