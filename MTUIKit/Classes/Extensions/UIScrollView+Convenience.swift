//
//  UIScrollView+Convenience.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 27.02.19.
//

import Foundation
import UIKit

public extension UIScrollView {
    
    /// Scrolls scollview so that the passed subview
    /// ends up in the top third of the scrollview
    ///
    /// - Parameter subview: subview to scroll to
    func scrollToVisibleRect(subview: UIView) {
        let frame = self.frame(ofSubview: subview)
        self.setContentOffset(CGPoint.init(
            x: 0, y: frame.origin.y - (self.frame.size.height / 4)), animated: true)
    }
    
    /// Returns a subview's flattened frame in relation to the scrollview
    ///
    /// - Parameter view: subview of the scrollview
    /// - Returns: frame
    private func frame(ofSubview view: UIView?) -> CGRect {
        guard let view = view else {
            return CGRect.zero
        }
        
        if view.superview == self {
            return view.frame
        } else {
            let frame = self.frame(ofSubview: view.superview)
            return CGRect.init(x: view.frame.origin.x + frame.origin.x,
                               y: view.frame.origin.y + frame.origin.y,
                               width: view.frame.size.width,
                               height: view.frame.size.height)
        }
    }
  
}
