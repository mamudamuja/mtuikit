//
//  UIView+Convenience.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 27.02.19.
//

import UIKit

public extension UIView {

    /// Convenience function for removing all subviews from a view
    func removeAllSubviews() {
        for view in self.subviews {
            view.removeFromSuperview()
        }
    }
    
    /// Creates a snapshot of the view
    ///
    /// - Returns: snapshot of the view
    func snapshot() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
}
