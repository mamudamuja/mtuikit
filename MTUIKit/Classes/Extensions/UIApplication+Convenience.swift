//
//  UIApplication+Convenience.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 14.03.19.
//

import UIKit

public extension UIApplication {
    
    /// Opens the url which opens up the subscription management screen in the App Store
    func manageSubscriptions() {
        if let url = URL.init(string: "https://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/manageSubscriptions") {
            self.open(url, options: [:], completionHandler: nil)
        }
    }
    
}
