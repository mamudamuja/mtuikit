//
//  UIAlertController+Convenience.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 22.02.20.
//

import UIKit

public extension UIAlertController {
    
    /// Returns an instance of the UIAlertController with 'alert' style.
    /// - Parameters:
    ///   - title: title of the controller
    ///   - message: message of the controller
    ///   - actions: actions (defaults to a single cancel action that dismisses the controller)
    static func alertController(
        withTitle title: String,
        message: String,
        actions: [UIAlertAction]? = nil) -> UIAlertController {
        
        // init alert controller
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // default cancel action
        let cancelAction = UIAlertAction(
            title: "Cancel",
            style: .cancel) { (action) in
                alertController.presentingViewController?.dismiss(animated: true, completion: nil)
        }
        
        let alertActions = actions ?? [cancelAction]
        for action in alertActions {
            alertController.addAction(action)
        }
        
        return alertController
    }
    
}
