//
//  Array+Convenience.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 21.02.20.
//

import Foundation

public extension Array {
    
    func elementAtIndex(index: Index) -> Element? {
        if index < self.count {
            return self[index]
        }
        
        return nil
    }
    
}
