
//
//  UICollectionView+Convenience.swift
//  AnimatedCollectionViewLayout
//
//  Created by Milutin Tomic on 26.02.19.
//

import UIKit

extension UICollectionView {
  
  public func registerReusableCell<T: UICollectionViewCell>(_: T.Type)
    where T: Reusable {
      if let nib = T.nib {
        self.register(nib, forCellWithReuseIdentifier: T.reuseIdentifier)
      } else {
        self.register(T.self, forCellWithReuseIdentifier: T.reuseIdentifier)
      }
  }
  
  public func dequeueReusableCell<T: UICollectionViewCell>(
    indexPath: IndexPath) -> T where T: Reusable {
    return self.dequeueReusableCell(
      withReuseIdentifier: T.reuseIdentifier,
      for: indexPath) as! T // swiftlint:disable:this force_cast
  }
  
}
