//
//  UIView+Autolayout.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 18.05.18.
//

import Foundation

public extension UIView {
    
    // MARK: - Autolayout
    
    /// Sets up necessary constraints so that the receiving view is pinned
    /// to the edges of it's superview
    ///
    /// - Parameter insets: specifies the insets from each edge
    /// - Returns: the list of added constraints or nil
    @discardableResult func pinEdges(
        edges: UIRectEdge = .all,
        insets: UIEdgeInsets? = nil) -> [NSLayoutConstraint]? {
        guard let superView = self.superview else {
            return nil
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        var constraints: [NSLayoutConstraint] = []
        
        if edges.contains(.left) {
            constraints.append(contentsOf: NSLayoutConstraint.constraints(
                withVisualFormat: "H:|-left-[view]",
                options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                metrics: ["left": insets?.left ?? 0],
                views: ["view": self]))
        }
        
        if edges.contains(.right) {
            constraints.append(contentsOf: NSLayoutConstraint.constraints(
                withVisualFormat: "H:[view]-right-|",
                options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                metrics: ["right": insets?.right ?? 0],
                views: ["view": self]))
        }
        
        if edges.contains(.top) {
            constraints.append(contentsOf: NSLayoutConstraint.constraints(
                withVisualFormat: "V:|-top-[view]",
                options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                metrics: ["top": insets?.top ?? 0],
                views: ["view": self]))
        }
        
        if edges.contains(.bottom) {
            constraints.append(contentsOf: NSLayoutConstraint.constraints(
                withVisualFormat: "V:[view]-bottom-|",
                options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                metrics: ["bottom": insets?.bottom ?? 0],
                views: ["view": self]))
        }
        
        superView.addConstraints(constraints)
        return constraints
    }
    
    func pinTopToSuperView(padding: Float = 0) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.superview?.addConstraints(NSLayoutConstraint
            .constraints(withVisualFormat: "V:|-top-[view]",
                         options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                         metrics: ["top": padding],
                         views: ["view": self]))
    }
    
    func pinBottomToSuperView(padding: Float = 0) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.superview?.addConstraints(NSLayoutConstraint
            .constraints(withVisualFormat: "V:[view]-bottom-|",
                         options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                         metrics: ["bottom": padding],
                         views: ["view": self]))
    }
    
    @discardableResult
    func pinBottomToSafeArea(view: UIView, offset: CGFloat = 0.0) -> NSLayoutConstraint {
        self.translatesAutoresizingMaskIntoConstraints = false
        let margins = view.layoutMarginsGuide
        
        let constraint = margins.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: offset)
        NSLayoutConstraint.activate([
            constraint
        ])
        
        return constraint
    }
    
    func pinTopToSafeArea(view: UIView, offset: CGFloat = 0.0) {
        self.translatesAutoresizingMaskIntoConstraints = false
        let margins = view.layoutMarginsGuide
        NSLayoutConstraint.activate([
            margins.topAnchor.constraint(equalTo: self.topAnchor, constant: offset)
        ])
    }
    
    func pinLeadingToSuperView(padding: Float = 0) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.superview?.addConstraints(NSLayoutConstraint
            .constraints(withVisualFormat: "H:|-lead-[view]",
                         options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                         metrics: ["lead": padding],
                         views: ["view": self]))
    }
    
    func pinTrailingToSuperView(padding: Float = 0, priority: Float = 1000) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.superview?.addConstraints(NSLayoutConstraint
            .constraints(withVisualFormat: "H:[view]-trail@prio-|",
                         options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                         metrics: ["trail": padding,
                                   "prio": priority],
                         views: ["view": self]))
    }
    
    func oneToOneAspectRatio() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraint(NSLayoutConstraint.init(
            item: self,
            attribute: .height,
            relatedBy: .equal,
            toItem: self,
            attribute: .width,
            multiplier: 1.0,
            constant: 0.0))
    }
    
    func centerInSuperView(offset: CGPoint? = nil) {
        centerHorizontallyInSuperView(offset: offset?.x ?? 0)
        centerVerticallyInSuperView(offset: offset?.y ?? 0)
    }
    
    func centerVerticallyInSuperView(offset: CGFloat? = nil) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.superview?.addConstraint(NSLayoutConstraint.init(
            item: self,
            attribute: .centerY,
            relatedBy: .equal,
            toItem: self.superview,
            attribute: .centerY,
            multiplier: 1,
            constant: offset ?? 0))
    }
    
    func centerHorizontallyInSuperView(offset: CGFloat? = nil) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.superview?.addConstraint(NSLayoutConstraint.init(
            item: self,
            attribute: .centerX,
            relatedBy: .equal,
            toItem: self.superview,
            attribute: .centerX,
            multiplier: 1,
            constant: offset ?? 0))
    }
    
    @discardableResult func constraintWidth(width: CGFloat) -> NSLayoutConstraint? {
        self.translatesAutoresizingMaskIntoConstraints = false
        let constraints = NSLayoutConstraint
            .constraints(withVisualFormat: "H:[view(==width)]",
                         options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                         metrics: ["width": width],
                         views: ["view": self])
        self.addConstraints(constraints)
        return self.constraints.first
    }
    
    func constraintHeight(height: CGFloat, priority: Float = 1000) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraints(NSLayoutConstraint
            .constraints(withVisualFormat: "V:[view(==height@prio)]",
                         options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                         metrics: ["height": height,
                                   "prio": priority],
                         views: ["view": self]))
    }
    
    func distributeVertically(
        views: [UIView]?,
        spaceBetweenViews: Double,
        insets: UIEdgeInsets = UIEdgeInsets.zero) {
        guard let subviews = views else { return }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        var previousView: UIView?
        for view in subviews {
            // add to view
            self.addSubview(view)
            
            // setup constraints
            view.translatesAutoresizingMaskIntoConstraints = false
            self.addConstraints(NSLayoutConstraint.constraints(
                withVisualFormat: "H:|-left-[view]-right-|",
                options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                metrics: ["left": insets.left,
                          "right": insets.right],
                views: ["view": view]))
            
            if let v = previousView {
                self.addConstraints(NSLayoutConstraint.constraints(
                    withVisualFormat: "V:[previous]-spacing-[view]",
                    options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                    metrics: ["spacing": spaceBetweenViews],
                    views: ["view": view,
                            "previous": v]))
            } else {
                self.addConstraints(NSLayoutConstraint.constraints(
                    withVisualFormat: "V:|-top-[view]",
                    options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                    metrics: ["top": insets.top],
                    views: ["view": view]))
            }
            
            previousView = view
        }
        
        // setup last constraint
        if let v = previousView {
            self.addConstraints(NSLayoutConstraint.constraints(
                withVisualFormat: "V:[view]-bottom-|",
                options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                metrics: ["bottom": insets.bottom],
                views: ["view": v]))
        }
    }
    
    func distributeHorizontally(
        views: [UIView]?,
        spaceBetweenViews: Double,
        insets: UIEdgeInsets = UIEdgeInsets.zero) {
        guard let subviews = views else { return }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        var previousView: UIView?
        for view in subviews {
            // add to view
            self.addSubview(view)
            
            // setup constraints
            view.translatesAutoresizingMaskIntoConstraints = false
            self.addConstraints(NSLayoutConstraint.constraints(
                withVisualFormat: "V:|-top-[view]-bottom-|",
                options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                metrics: ["top": insets.top,
                          "bottom": insets.bottom],
                views: ["view": view]))
            
            if let v = previousView {
                self.addConstraints(NSLayoutConstraint.constraints(
                    withVisualFormat: "H:[previous]-spacing-[view]",
                    options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                    metrics: ["spacing": spaceBetweenViews],
                    views: ["view": view,
                            "previous": v]))
            } else {
                self.addConstraints(NSLayoutConstraint.constraints(
                    withVisualFormat: "H:|-left-[view]",
                    options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                    metrics: ["left": insets.left],
                    views: ["view": view]))
            }
            
            previousView = view
        }
        
        // setup last constraint
        if let v = previousView {
            self.addConstraints(NSLayoutConstraint.constraints(
                withVisualFormat: "H:[view]-right-|",
                options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                metrics: ["right": insets.right],
                views: ["view": v]))
        }
    }
    
    @discardableResult
    func addConstraints(
        withHorizontalVFL horizontalVFL: String? = nil,
        verticalVFL: String? = nil,
        views: [String: UIView]) -> [NSLayoutConstraint]? {
        // MTTODO: all functions here should return the resulting constrain(s)
        
        for view in views.values {
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        var constraints: [NSLayoutConstraint] = []
        
        if let horizontalString = horizontalVFL {
            let finalFormat = "H:\(horizontalString)"
            let horinzontalConstrains = NSLayoutConstraint.constraints(
                withVisualFormat: finalFormat,
                options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                metrics: nil,
                views: views)
            constraints.append(contentsOf: horinzontalConstrains)
            self.addConstraints(horinzontalConstrains)
        }
        
        if let verticalString = verticalVFL {
            let finalFormat = "V:\(verticalString)"
            let verticalConstrains = NSLayoutConstraint.constraints(
                withVisualFormat: finalFormat,
                options: NSLayoutConstraint.FormatOptions.init(rawValue: 0),
                metrics: nil,
                views: views)
            constraints.append(contentsOf: verticalConstrains)
            self.addConstraints(verticalConstrains)
        }
        
        return constraints
    }
    
}
