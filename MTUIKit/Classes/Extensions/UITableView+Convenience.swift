//
//  UITableView+Convenience.swift
//  AnimatedCollectionViewLayout
//
//  Created by Milutin Tomic on 26.02.19.
//

import UIKit

extension UITableView {
  
    public func registerReusableCell<T: UITableViewCell>(_: T.Type) where T: Reusable {
        if let nib = T.nib {
            self.register(nib, forCellReuseIdentifier: T.reuseIdentifier)
        } else {
            self.register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
        }
    }
    
    public func dequeueReusableCell<T: UITableViewCell>(
        indexPath: IndexPath) -> T where T: Reusable {
        return self.dequeueReusableCell(
            withIdentifier: T.reuseIdentifier, for: indexPath)
            as! T // swiftlint:disable:this force_cast
    }
  
}
