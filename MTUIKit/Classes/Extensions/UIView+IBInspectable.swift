//
//  UIView+MTUIKit.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 18.05.18.
//

import Foundation

/// This extension adds some conveniece (IBInspectable) properties
/// making it possible to edit them directly in the Interface Builder
extension UIView {
  
  // MARK: - Layer
  
  /// The corner radius of the view's layer
  @IBInspectable open var cornerRadius: CGFloat {
    get {
      return layer.cornerRadius
    }
    set {
      layer.cornerRadius = newValue
    }
  }
  
  // MARK: - Border
  
  /// The border width of the view
  @IBInspectable open var borderWidth: CGFloat {
    get {
      return layer.borderWidth
    }
    set {
      layer.borderWidth = newValue
    }
  }
  
  /// The border color of the view
  @IBInspectable open var borderColor: UIColor {
    get {
      return UIColor.init(cgColor: layer.borderColor!)
    }
    set {
      layer.borderColor = newValue.cgColor
    }
  }
  
  // MARK: - Shadow
  
  /// The shadow color of the view
  /// Setting this value adds a shadow path to the view's layer
  @IBInspectable open var shadowColor: UIColor? {
    get {
      if let color = layer.shadowColor {
        return UIColor.init(cgColor: color)
      } else {
        return nil
      }
    }
    set {
      // set a shadow path
      // this improves the performance drastically
      layer.shadowPath = UIBezierPath
        .init(roundedRect: self.bounds,
              cornerRadius: self.cornerRadius).cgPath
      
      // set new shadow color
      layer.shadowColor = newValue?.cgColor
    }
  }
  
  /// The horizontal and vertical offset of the shadow
  @IBInspectable open var shadowOffset: CGSize {
    get {
      return layer.shadowOffset
    }
    set {
      layer.shadowOffset = newValue
    }
  }
  
  /// The shadow radius
  @IBInspectable open var shadowRadius: CGFloat {
    get {
      return layer.shadowRadius
    }
    set {
      layer.shadowRadius = newValue
    }
  }
  
  /// The shadow opacity
  @IBInspectable open var shadowOpacity: Float {
    get {
      return layer.shadowOpacity
    }
    set {
      layer.shadowOpacity = newValue
    }
  }
  
}
