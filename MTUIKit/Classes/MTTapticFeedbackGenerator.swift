//
//  MTTapticFeedbackGenerator.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 28.02.19.
//

import UIKit

public final class MTTapticFeedbackGenerator {
  
  /// Uses impact feedback generators to indicate that an impact has occurred.
  /// For example, you might trigger impact feedback when a user
  /// interface object collides with something or snaps into place.
  public static func generateImpactFeedback(
    style: UIImpactFeedbackGenerator.FeedbackStyle) {
    let generator = UIImpactFeedbackGenerator.init(style: style)
    generator.impactOccurred()
  }
  
  /// Uses notification feedback to communicate that a task or action
  /// has succeeded, failed, or produced a warning of some kind.
  public static func generateNotificationFeedback(
    type: UINotificationFeedbackGenerator.FeedbackType) {
    let generator = UINotificationFeedbackGenerator.init()
    generator.notificationOccurred(type)
  }
  
}
