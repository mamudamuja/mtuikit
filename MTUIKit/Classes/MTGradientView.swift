//
//  MTGradientView.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 13.03.20.
//

import UIKit

/// A simple gradient view. Displays a linear 2 color gradient.
/// View is designable in the Interface Builder.
@IBDesignable
public class MTGradientView: UIView {
    
    var gradientLayer: CAGradientLayer?
    
    @IBInspectable var firstColor: UIColor? {
        didSet {
            self.refreshGradient()
        }
    }
    
    @IBInspectable var secondColor: UIColor? {
        didSet {
            self.refreshGradient()
        }
    }
    
    @IBInspectable var startPoint: CGPoint = CGPoint.init(x: 0.5, y: 0) {
        didSet {
            self.refreshGradient()
        }
    }
    
    @IBInspectable var endPoint: CGPoint = CGPoint.init(x: 0.5, y: 1) {
        didSet {
            self.refreshGradient()
        }
    }
    
    // MARK: - Overrides
    
    public convenience init(
        startPoint: CGPoint,
        endPoint: CGPoint,
        firstColor: UIColor,
        secondColor: UIColor) {
        self.init(frame: .zero)
        self.startPoint = startPoint
        self.endPoint = endPoint
        self.firstColor = firstColor
        self.secondColor = secondColor
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        self.refreshGradient()
    }
    
    // MARK: - Private
    
    /// Sets up the gradient layer
    private func refreshGradient() {
        guard let firstColor = self.firstColor else {
            return
        }
        
        guard let secondColor = self.secondColor else {
            return
        }
        
        if self.gradientLayer != nil {
            self.gradientLayer?.removeFromSuperlayer()
        }
        
        self.gradientLayer = CAGradientLayer.init()
        self.gradientLayer?.frame = self.bounds
        self.gradientLayer?.colors = [firstColor.cgColor,
                                      secondColor.cgColor]
        self.gradientLayer?.startPoint = startPoint
        self.gradientLayer?.endPoint = endPoint
        self.layer.insertSublayer(self.gradientLayer!, at: 0)
    }
    
}

