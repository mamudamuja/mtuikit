//
//  MTAnimatedCollectionViewLayout.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 07.02.18.
//

import Foundation
import UIKit

public class MTAnimatedCollectionViewLayout: UICollectionViewFlowLayout {
  
  /// animator responsible for the transition animations
  public var animator: MTLayoutAnimator?
  
  // MARK: - Overrides
  
  /// override the class used to store layout attributes
  public override class var layoutAttributesClass: AnyClass {
    return MTAnimatedCollectionViewLayoutAttributes.self
  }
  
  override public func layoutAttributesForElements(
    in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    // get from parent
    guard let attributes = super
      .layoutAttributesForElements(in: rect) else { return nil }
    
    let mtAttributesMap = attributes.compactMap({ (layout) ->
      MTAnimatedCollectionViewLayoutAttributes? in
      return layout.copy() as? MTAnimatedCollectionViewLayoutAttributes
    })
    
    // transform attributes and return
    return mtAttributesMap.map { (layout) ->
      UICollectionViewLayoutAttributes in
      return self.transformLayoutAttributes(layout)
    }
  }
  
  public override func shouldInvalidateLayout(
    forBoundsChange newBounds: CGRect) -> Bool {
    return true
  }
  
  // MARK: - Private
  
  /// Transforms the passed attributes using the animator
  ///
  /// - Parameter attributes: attributes to transform
  /// - Returns: transformed attributes
  private func transformLayoutAttributes(
    _ attributes: MTAnimatedCollectionViewLayoutAttributes) ->
    UICollectionViewLayoutAttributes {
      // get parent collection view
      guard let collectionView = self.collectionView else {
        return attributes
      }
      
      let distance: CGFloat
      let itemOffset: CGFloat
      
      // calculate attributes
      if self.scrollDirection == .horizontal {
        distance = collectionView.frame.width
        itemOffset =
          attributes.center.x -
          collectionView.contentOffset.x
        attributes.startOffset =
          (attributes.frame.origin.x -
            collectionView.contentOffset.x) / attributes.frame.width
      } else {
        distance = collectionView.frame.height
        itemOffset = attributes.center.y -
          collectionView.contentOffset.y
        attributes.startOffset =
          (attributes.frame.origin.y -
            collectionView.contentOffset.y) /
          attributes.frame.height
        attributes.endOffset =
          (attributes.frame.origin.y -
            collectionView.contentOffset.y -
            collectionView.frame.height) / attributes.frame.height
      }
      
      attributes.scrollDirection = self.scrollDirection
      attributes.middleOffset = itemOffset / distance - 0.5
      
      // cache the content view of the cell
      if attributes.contentView == nil,
        let cell = collectionView.cellForItem(
          at: attributes.indexPath)?.contentView {
        attributes.contentView = cell
      }
      
      // animate transition
      self.animator?.animate(
        collectionView: collectionView,
        attributes: attributes)
      
      return attributes
  }
  
}

/// A UICollectionViewLayoutAttributes subclass that stores some additional
/// properties required for the transition animation
public class MTAnimatedCollectionViewLayoutAttributes:
UICollectionViewLayoutAttributes {
  
  /// used to chache the contentView of
  /// the cell the attributes belong to
  public var contentView: UIView?
  
  /// stores the scrollDirection of the collection view.
  /// Defaults to 'vertical'.
  public var scrollDirection: UICollectionView.ScrollDirection = .vertical
  
  /// The ratio of the distance between the start of the cell and the start
  /// of the collectionView and the height/width of the cell depending
  /// on the scrollDirection. It's 0 when the start of the cell aligns
  /// the start of the collectionView. It gets positive when the cell
  /// moves towards the scrolling direction (right/down) while getting
  /// negative when moves opposite.
  public var startOffset: CGFloat = 0
  
  /// The ratio of the distance between the center of the cell and the center
  /// of the collectionView and the height/width of the cell depending on
  /// the scrollDirection. It's 0 when the center of the cell aligns the
  /// center of the collectionView. It gets positive when the cell moves
  /// towards the scrolling direction (right/down) while getting negative
  /// when moves opposite.
  public var middleOffset: CGFloat = 0
  
  /// The ratio of the distance between the **start** of the cell and the
  /// end of the collectionView and the height/width of the cell depending
  /// on the scrollDirection. It's 0 when the **start** of the cell aligns
  /// the end of the collectionView. It gets positive when the cell moves
  /// towards the scrolling direction (right/down) while getting negative
  /// when moves opposite.
  public var endOffset: CGFloat = 0
  
  // MARK: - Overrides
  
  public override func copy(with zone: NSZone? = nil) -> Any {
    if let copy = super.copy(with: zone)
      as? MTAnimatedCollectionViewLayoutAttributes {
      copy.contentView = self.contentView
      copy.scrollDirection = self.scrollDirection
      copy.startOffset = self.startOffset
      copy.middleOffset = self.middleOffset
      copy.endOffset = self.endOffset
      return copy
    }
    
    return MTAnimatedCollectionViewLayoutAttributes.init()
  }
  
  public override func isEqual(_ object: Any?) -> Bool {
    guard let obj = object as?
      MTAnimatedCollectionViewLayoutAttributes else {
        return false
    }
    
    return super.isEqual(obj) &&
      obj.contentView == self.contentView &&
      obj.scrollDirection == self.scrollDirection &&
      obj.startOffset == self.startOffset &&
      obj.middleOffset == self.middleOffset &&
      obj.endOffset == self.endOffset
  }
  
}
