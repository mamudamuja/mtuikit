//
//  MTPopSegue.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 28.02.19.
//

import UIKit

class MTPopSegue: UIStoryboardSegue {

  public override func perform() {
    self.source.navigationController?.popViewController(animated: true)
  }
  
}
