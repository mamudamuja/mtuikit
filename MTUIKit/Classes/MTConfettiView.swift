//
//  MTConfettiView.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 09.03.19.
//

import UIKit

public class MTConfettiView: UIView {
  
  // MARK: - API
  
  public var particles: [UIImage] = []
  
  /// the origin of the particles
  public var emissionOrigin: CGRect = CGRect.zero {
    didSet {
      self.positionLayer()
    }
  }
  
  /// Emits conffeti particles
  ///
  /// - Parameters:
  ///   - duration: duration of the emission, if 0 will emit endlessly
  ///   - birthRate: particles emitted per second
  public func emit(duration: TimeInterval = 0, birthRate: Float = 20.0) {
    self.setup(birthRate: birthRate)
    
    // stop emisison if duration parameter was passed
    if duration > 0 {
      // stop emission after duration expired
      DispatchQueue.main.asyncAfter(deadline: .now() + duration, execute: {
        self.emitterLayer?.birthRate = 0
      })
    }
  }
  
  // MARK: - Overrides
  
  public override func hitTest(
    _ point: CGPoint, with event: UIEvent?) -> UIView? {
    return nil
  }
  
  //  override func layoutSubviews() {
  //    super.layoutSubviews()
  //    positionLayer()
  //  }
  
  // MARK: - Private
  
  /// the layer instance emitting the confetti particles
  private var emitterLayer: CAEmitterLayer?
  
  /// stores the confetti emitter cells
  private var emitterCells: [CAEmitterCell] = []
  
  /// Configures the emitter cells
  private func setup(birthRate: Float) {
    self.emitterLayer = CAEmitterLayer.init()
    self.emitterLayer?.preservesDepth = true
    
    let birthRatePerCell = birthRate == 0 ? 0 :
      birthRate / Float(particles.count)
    
    self.emitterCells = particles.map { (particle) -> CAEmitterCell in
      let cell = CAEmitterCell.init()
      cell.scale = 2.0
      cell.scaleRange = 0.2
      cell.emissionRange = CGFloat(Double.pi / 2)
      cell.emissionLongitude = CGFloat(Double.pi / 2)
      cell.spin = 10
      cell.spinRange = CGFloat(Double.pi / 6)
      cell.lifetime = 5.0
      cell.birthRate = birthRatePerCell
      cell.velocity = -100
      cell.yAcceleration = 150
      cell.contents = particle.cgImage
      return cell
    }
    
    self.emitterLayer?.emitterCells = self.emitterCells
//    self.emitterLayer?.emitterShape = "sphere"
    
    guard let subLayer = self.emitterLayer else { return }
    self.layer.addSublayer(subLayer)
    
    self.positionLayer()
  }
  
  /// positions the layer
  private func positionLayer() {
    self.emitterLayer?.emitterPosition = CGPoint(
      x: self.emissionOrigin.origin.x + self.emissionOrigin.size.width / 2.0,
      y: self.emissionOrigin.origin.y + self.emissionOrigin.size.height / 2.0)
    self.emitterLayer?.emitterSize = self.emissionOrigin.size
  }
  
}

