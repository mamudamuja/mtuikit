//
//  MTDismissSegue.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 27.02.19.
//

import UIKit

public class MTDismissSegue: UIStoryboardSegue {

  public override func perform() {
    self.source.presentingViewController?
      .dismiss(animated: true, completion: nil)
  }
  
}
