//
//  MTNibView.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 27.02.19.
//

import UIKit

open class MTNibView: UIView {

  var view: UIView!

  public override init(frame: CGRect) {
    super.init(frame: frame)
    self.xibSetup()
  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.xibSetup()
  }
  
}

private extension MTNibView {
  func xibSetup() {
    self.backgroundColor = UIColor.clear
    self.view.loadNib()
    self.view.frame = self.bounds
    self.addSubview(self.view)
    self.view.translatesAutoresizingMaskIntoConstraints = false
    self.view.pinEdges()
  }
}
