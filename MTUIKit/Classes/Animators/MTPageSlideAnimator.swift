//
//  MTPageSlideAnimator.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 07.02.18.
//

import UIKit

public class MTPageSlideAnimator: NSObject, MTLayoutAnimator {
    
    // MARK: - Attributes
    
    /// The max scale that should be applied to the front most cell.
    /// 0.0 -> min scale
    /// 0.5 -> max scale
    /// 0.2 -> default
    private var scaleRate: CGFloat
    
    // MARK: - Initializers
    
    public init(scaleRate: CGFloat = 0.2) {
        // clamp value
        self.scaleRate = min(max(scaleRate, 0.0), 0.5)
    }
    
    // MARK: - MTLayoutAnimator
    
    public func animate(
        collectionView: UICollectionView,
        attributes: MTAnimatedCollectionViewLayoutAttributes) {
        let position = attributes.middleOffset
        let contentOffset = collectionView.contentOffset
        let itemOrigin = attributes.frame.origin
        let scaleFactor = self.scaleRate * min(position, 0) + 1.0
        var transform = CGAffineTransform.init(
            scaleX: scaleFactor, y: scaleFactor)

        if attributes.scrollDirection == .horizontal {
             transform = transform.concatenating(
                CGAffineTransform(
                    translationX: position < 0 ? contentOffset.x -
                        itemOrigin.x : 0,
                    y: 0))
        } else {
            transform = transform.concatenating(
                CGAffineTransform(
                    translationX: 0,
                    y: position < 0 ? contentOffset.y - itemOrigin.y : 0))
        }
        
        // apply transform
        attributes.transform = transform
        attributes.zIndex = attributes.indexPath.row
    }
    
}
