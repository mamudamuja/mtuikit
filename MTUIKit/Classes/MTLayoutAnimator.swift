//
//  MTLayoutAnimator.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 07.02.18.
//

import UIKit

/// This is a protocol all the different animators have to conform to
public protocol MTLayoutAnimator {
    
    /// The function responsible for transition animation.
    ///
    /// - Parameters:
    ///   - collectionView: target collection view
    ///   - attributes: attributes to be animated
    func animate(
      collectionView: UICollectionView,
      attributes: MTAnimatedCollectionViewLayoutAttributes)
    
}
