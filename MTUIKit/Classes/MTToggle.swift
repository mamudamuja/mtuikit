//
//  MTToggle.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 16.03.19.
//

import UIKit

@IBDesignable
open class MTToggle: UIControl {

  // MARK: - API
  
  @IBInspectable
  public var isOn: Bool = false {
    didSet {
      self.updateSubviews()
    }
  }
  
  @IBInspectable
  public var onIcon: UIImage?
  
  @IBInspectable
  public var offIcon: UIImage?
  
  public override init(frame: CGRect) {
    super.init(frame: frame)
    self.setupView()
  }
  
  public required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.setupView()
  }
  
  open override var intrinsicContentSize: CGSize {
    return self.iconView.intrinsicContentSize
  }
  
  // MARK: - Private
  
  private lazy var iconView: UIImageView = {
    let imageView = UIImageView.init()
    imageView.setContentCompressionResistancePriority(
      .required, for: .horizontal)
    imageView.setContentCompressionResistancePriority(
      .required, for: .vertical)
    imageView.setContentHuggingPriority(.required, for: .vertical)
    imageView.setContentHuggingPriority(.required, for: .horizontal)
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.contentMode = .scaleAspectFit
    return imageView
  }()
  
  @objc
  private func handleToggle() {
    self.isOn = !self.isOn
    self.sendActions(for: .valueChanged)
    self.updateSubviews()
  }
  
  open override func layoutSubviews() {
    self.updateSubviews()
  }
  
  private func updateSubviews() {
    self.iconView.image = self.isOn ? self.onIcon : self.offIcon
  }
  
  private func setupView() {
    // add touch recognizer
    let touchRecognizer = UITapGestureRecognizer.init(
      target: self,
      action: #selector(handleToggle))
    self.addGestureRecognizer(touchRecognizer)
    
    // setup subviews
    self.addSubview(self.iconView)
    
    // layout subviews
    self.iconView.pinEdges()
  }
  
}
