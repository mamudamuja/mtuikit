//
//  Reuseable.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 26.02.19.
//

import UIKit

public protocol Reusable: class {
  static var reuseIdentifier: String { get }
  static var nib: UINib? { get }
}

public extension Reusable {
  static var reuseIdentifier: String {
    return String.init(describing: self)
  }
  
  static var nib: UINib? {
    return UINib.init(nibName: self.reuseIdentifier, bundle: nil)
  }
}
