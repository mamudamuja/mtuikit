//
//  MTTextField.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 27.02.19.
//

import UIKit

open class MTTextField: UITextField, UITextFieldDelegate {
  
  @IBOutlet public weak var nextField: UITextField?
  @IBOutlet public weak var scrollView: UIScrollView?
  
  public func isValidEmailAddress() -> Bool {
    var returnValue = true
    let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
    
    guard let content = self.text else { return false }
    
    do {
      let regex = try NSRegularExpression(pattern: emailRegEx)
      let nsString = content as NSString
      let results = regex.matches(
        in: content, range: NSRange(location: 0, length: nsString.length))
      
      if results.count == 0 {
        returnValue = false
      }
    } catch let error as NSError {
      print("invalid regex: \(error.localizedDescription)")
      returnValue = false
    }
    
    return  returnValue
  }
  
}

