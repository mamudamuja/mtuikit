//
//  MTButton.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 07.12.19.
//

import Foundation
import UIKit

public class MTButton: UIButton {
    
    public var onButtonDown: (() -> Void)?
    public var onButtonUpInside: (() -> Void)?
    public var onButtonUpOutside: (() -> Void)?
    public var onCancel: (() -> Void)?
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    // MARK: - Layout
    
    func setup() {
        self.addTarget(self, action: #selector(touchDown), for: .touchDown)
        self.addTarget(self, action: #selector(touchUpInside), for: .touchUpInside)
        self.addTarget(self, action: #selector(touchUpOutside), for: .touchUpOutside)
        self.addTarget(self, action: #selector(touchCancel), for: .touchCancel)
    }
    
    @objc private func touchDown() {
        onButtonDown?()
    }
    
    @objc private func touchUpInside() {
        onButtonUpInside?()
    }
    
    @objc private func touchUpOutside() {
        onButtonUpOutside?()
    }
    
    @objc private func touchCancel() {
        onCancel?()
    }
    
}
