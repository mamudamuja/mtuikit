//
//  MTPageControl.swift
//  MTUIKit
//
//  Created by Milutin Tomic on 26.02.19.
//

import UIKit

public class MTPageControll: UIPageControl, UIScrollViewDelegate {
  
  @IBOutlet weak var scrollView: UIScrollView? {
    didSet {
      self.scrollView?.delegate = self
    }
  }
  
  // MARK: - UIScrollViewDelegate
  
  public func scrollViewDidScroll(_ scrollView: UIScrollView) {
    guard let scrollView = self.scrollView else { return }
    let width = scrollView.frame.width
    guard width != 0 else { return }
    
    let newPage = Int(round(scrollView.contentOffset.x / width))
    if self.currentPage != newPage {
      self.currentPage = newPage
    }
  }
  
}

